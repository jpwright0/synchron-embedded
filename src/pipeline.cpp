#include "pipeline.hpp"

void run_pipeline(array<array<float, W_LEN>, W_CH>& a) {
    // compute all nodes
    node_multiply nb(2.0);
    nb.process(a);
    //test_matrix_cvalue(a, 2.0, 'b');

    array<array<float, W_LEN>, W_CH> a1 = a;

    node_add nc(-3.0);
    nc.process(a);
    //test_matrix_cvalue(a1, -1.0, 'c');
    node_add nd(4.0);
    nd.process(a1);
    //test_matrix_cvalue(a2, 6.0, 'd');

    node_sum ne(a1);
    ne.process(a);
}

void run_pipeline_optimized(array<array<float, W_LEN>, W_CH>& a) {
    node_addmultiply ne(4.0, 1.0);
    ne.process(a);
}