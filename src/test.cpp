#include "test.hpp"

#include <iostream>

using std::array;
using std::cout;
using std::size_t;

void print_matrix(array<array<float, W_LEN>, W_CH>& a) {
    cout << "[\r\n";
    for (size_t i = 0; i < 5; i++) {
        cout << " ";
        for (size_t j = 0; j < 5; j++) {
            printf("%1.2f ", a[i][j]);
        }
        cout << " ... \r\n";
    }
    cout << " ...\r\n]\r\n";
}

bool check_matrix_cvalue(array<array<float, W_LEN>, W_CH>& a, float v) {
    bool b = true;
    for (size_t i = 0; i < a.size(); i++) {
        for (size_t j = 0; j < a[i].size(); j++) {
            if (a[i][j] != v) b = false;
        }
    }
    return b;
}

void test_matrix_cvalue(array<array<float, W_LEN>, W_CH>& a, float v, char n) {
    auto b = check_matrix_cvalue(a, v);
    printf("test node %c for constant value %1.2f: %s\r\n", n, v, b ? "PASS" : "FAIL");
}