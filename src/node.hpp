#ifndef NODE_H__
#define NODE_H__

#include <array>

using std::array;

static constexpr int const W_LEN = 2000;
static constexpr int const W_CH = 16;

class node {
    public:
        virtual void process(array<array<float, W_LEN>, W_CH>& a) = 0;
};

#endif // NODE_H__