#include "nodemultiply.hpp"

using std::size_t;

node_multiply::node_multiply(float f) {
    coeff_ = f;
}

void node_multiply::process(array<array<float, W_LEN>, W_CH>& a) {
    for (size_t i = 0; i < a.size(); i++) {
        for (size_t j = 0; j < a[i].size(); j++) {
            a[i][j] *= coeff_;
        }
    }
}