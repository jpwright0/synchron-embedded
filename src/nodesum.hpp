#ifndef NODE_SUM_H__
#define NODE_SUM_H__

#include "node.hpp"

class node_sum : node {
	array<array<float, W_LEN>, W_CH>& addend;

    public:
    	node_sum(array<array<float, W_LEN>, W_CH>& f) : addend(f) { }
        void process(array<array<float, W_LEN>, W_CH>& a);
};

#endif // NODE_SUM_H__