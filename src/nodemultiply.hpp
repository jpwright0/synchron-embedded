#ifndef NODE_MULTIPLY_H__
#define NODE_MULTIPLY_H__

#include "node.hpp"

class node_multiply : node {
	float coeff_;

    public:
    	node_multiply(float f);
        void process(array<array<float, W_LEN>, W_CH>& a);
};

#endif // NODE_MULTIPLY_H__