#include "nodeaddmultiply.hpp"

using std::size_t;

node_addmultiply::node_addmultiply(float c, float a) {
    coeff_ = c;
    addend_ = a;
}

void node_addmultiply::process(array<array<float, W_LEN>, W_CH>& a) {
    for (size_t i = 0; i < a.size(); i++) {
        for (size_t j = 0; j < a[i].size(); j++) {
            a[i][j] = (a[i][j] * coeff_) + addend_;
        }
    }
}