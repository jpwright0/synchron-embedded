#ifndef NODE_ADD_H__
#define NODE_ADD_H__

#include "node.hpp"

class node_add : node {
	float addend_;

    public:
    	node_add(float f);
        void process(array<array<float, W_LEN>, W_CH>& a);
};

#endif // NODE_ADD_H__