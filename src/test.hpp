#ifndef TEST_H__
#define TEST_H__

#include "node.hpp"

using std::array;

void print_matrix(array<array<float, W_LEN>, W_CH>& a);

bool check_matrix_cvalue(array<array<float, W_LEN>, W_CH>& a, float v);

void test_matrix_cvalue(array<array<float, W_LEN>, W_CH>& a, float v, char n);

#endif // TEST_H__