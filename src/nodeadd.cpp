#include "nodeadd.hpp"

using std::size_t;

node_add::node_add(float f) {
    addend_ = f;
}

void node_add::process(array<array<float, W_LEN>, W_CH>& a) {
    for (size_t i = 0; i < a.size(); i++) {
        for (size_t j = 0; j < a[i].size(); j++) {
            a[i][j] += addend_;
        }
    }
}