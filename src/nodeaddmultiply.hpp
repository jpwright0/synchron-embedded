#ifndef NODE_ADDMULTIPLY_H__
#define NODE_ADDMULTIPLY_H__

#include "node.hpp"

class node_addmultiply : node {
	float coeff_;
	float addend_;

    public:
    	node_addmultiply(float c, float a);
        void process(array<array<float, W_LEN>, W_CH>& a);
};

#endif // NODE_ADDMULTIPLY_H__