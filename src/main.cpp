#include "pipeline.hpp"
#include "test.hpp"

#include <array>
#include <chrono>
#include <iostream>

using std::array;
using std::chrono::high_resolution_clock;
using std::cout, std::cerr;
using namespace std::literals;
using std::size_t;
using std::string;

int main() {
    // generate initial array of 1's
    // array size is defined in node.hpp
    array<array<float, W_LEN>, W_CH> ai;
    for (size_t i = 0; i < ai.size(); i++) {
        std::fill_n(ai[i].begin(), ai[i].size(), 1.0);
    }
    test_matrix_cvalue(ai, 1.0, 'a');
    auto a = ai;

    // compare execution time between computing all nodes
    // and simplified version (4x - 1)
    a = ai;
    auto t1 = high_resolution_clock::now();
    run_pipeline(a);
    auto t2 = high_resolution_clock::now();
    test_matrix_cvalue(a, 5.0, 'e');
    printf("computing all nodes: %u us\r\n", (unsigned int)((t2 - t1)/1us));

    a = ai;
    t1 = high_resolution_clock::now();
    run_pipeline_optimized(a);
    t2 = high_resolution_clock::now();
    test_matrix_cvalue(a, 5.0, 'e');
    printf("simplified version: %u us\r\n", (unsigned int)((t2 - t1)/1us));

    return 0;
}