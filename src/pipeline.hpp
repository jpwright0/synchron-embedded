#ifndef PIPELINE_H__
#define PIPELINE_H__

#include "nodeadd.hpp"
#include "nodemultiply.hpp"
#include "nodesum.hpp"
#include "nodeaddmultiply.hpp"

#include <array>

using std::array;

extern void run_pipeline(array<array<float, W_LEN>, W_CH>& a);

extern void run_pipeline_optimized(array<array<float, W_LEN>, W_CH>& a);

#endif // PIPELINE_H__