# synchron-embedded

## Prerequisites

* [GNU Make](https://www.gnu.org/software/make/)
* A C++ compiler with support for C++17
* (optional) Python 3+ and dependencies `numpy` and `pybind11` for building the Python wrapper

This code has been tested using only:

* macOS Big Sur 11.2.2
    * GNU Make 3.81
    * clang 12.0.5 
    * Python 3.9.6
* Ubuntu Linux 20.10
    * GNU Make 4.3
    * g++ 10.3.0
    * Python 3.8.10

## Building and running

To compile and run the main program:

```
make
./pipeline
```

To compile and run the Python bindings:

```
cd py
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
make
python main.py
```

## Notes

1. A two-dimensional `std::array` is used for holding the value at each node. This is appropriate because the size of each node is specified to be fixed at (16 channels x 2000 samples). If the size needs to be variable (e.g. if the length of the window is not guaranteed to be exactly 1 second), `std::vector` would be a suitable alternative.

2. I chose to optimize for memory usage, and because this will run on an embedded platform, avoided dynamic memory allocation. So the abstract `node` class has a `process` function that takes an input array, then updates the input array in-place. If preserving the value of each node is important, the copy operator can be used.

3. The function `test_matrix_cvalue` is used to verify that all values in the 2D array are equal to a single value. The `main` function runs this test for nodes A and E in the case described in the challenge `README` (tests of the individual nodes in `pipeline.cpp` are commented out)

4. I realized that node E could be achieved more efficiently in a single iteration of the input array, as nodes B-E could be simplified to `(2x - 3) + (2x + 4) = 4x + 1`. So I created a `node_addmultiply` class to compute node E in a single step, and measured the difference in runtime between this approach and computing each individual node (this is also run in `main`)

5. For the Python wrapper, I considered the simple approach of writing a Python script that spawns a separate process and piping the array via `stdin`/`stdout`, but decided to go for a compiled binding for a better performing solution that avoids spawning another process and using additional memory.
    * However, the wrapper does copy the input data before processing and returns a modified result.
    * It should be possible to avoid the copy, but this would require understanding the internals of `py::array` a bit more in order to map between 2D `py::array` and the `std::array<std::array<float ...` approach; additionally this would require ensuring that Python does not garbage-collect the array memory during operation. So I leave this as future work.