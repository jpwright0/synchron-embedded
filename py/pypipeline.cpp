#include "pipeline.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
namespace py = pybind11;

using std::memcpy;
using std::runtime_error;

py::array py_run_pipeline(py::array_t<float, py::array::c_style | py::array::forcecast> array) {
    // code based on pybind11 example:
    // https://github.com/tdegeus/pybind11_examples/blob/master/04_numpy-2D_cpp-vector/example.cpp

    // check input dimensions
    if (array.ndim() != 2)
        throw runtime_error("Input should be 2-D NumPy array");
    if (array.shape()[0] != W_CH || array.shape()[1] != W_LEN)
        throw runtime_error("Input should have size equal to (W_CH, W_LEN)");

    std::array<std::array<float, W_LEN>, W_CH> a;

    // copy py::array -> std::array
    std::memcpy(a.data(),array.data(),array.size()*sizeof(float));

    // call pure C++ function
    run_pipeline(a);

    ssize_t              ndim    = 2;
    std::vector<ssize_t> shape   = { array.shape()[0] , 3 };
    std::vector<ssize_t> strides = { sizeof(float)*3 , sizeof(float) };

    // return 2-D NumPy array
    return py::array(py::buffer_info(
        a.data(),                               /* data as contiguous array  */
        sizeof(float),                          /* size of one scalar        */
        py::format_descriptor<float>::format(), /* data type                 */
        ndim,                                   /* number of dimensions      */
        shape,                                  /* shape of the matrix       */
        strides                                 /* strides for each axis     */
    ));
}

PYBIND11_MODULE(pypipeline, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring
    m.def("run_pipeline", &py_run_pipeline, "A function which adds two numbers");
}