import pypipeline
import numpy as np

def check_matrix_cvalue(a, v):
    return np.all(a == v)

def test_matrix_cvalue(a, v, n):
    b = check_matrix_cvalue(a, v)
    print("test node {0} for constant value {1}: {2}\r\n".format(n, v, ("PASS" if b else "FAIL")))

def main():
    l = np.empty(shape=(16,2000))
    l.fill(1.0)

    test_matrix_cvalue(l, 1.0, 'a')

    l = pypipeline.run_pipeline(l)

    test_matrix_cvalue(l, 5.0, 'e')

if __name__ == "__main__":
    main()